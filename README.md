# [Watch Together SDK](https://documentation.sceenic.co/)
Watch Together video-chat is a general-purpose video chatting infrastructure 
that is aimed to provide high-quality Audio and Video functionality
that will allow you to create and provide engaging experiences in your application for your customers.  

To register to our service please contact us on [Support@sceenic.co](mailto:Support@sceenic.co).

If you already have credentials to the private area, login and retrieve your `API_KEY` and `API_SECRET` - [Private area](https://media.sceenic.co)
Have a look at the [Watch-Together overview](https://documentation.sceenic.co/sscale-confluence-watch-together-overview), once done, you can [set up your Authentication server](https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-overview#customer-authentication-server) and start working.

**( * ) SDKs are accessible only from the private area**

Need technical support? contact us at [Support@sceenic.co](mailto:Support@sceenic.co)

## Documentation

Have a look at our official documentation site [Sceenic - WatchTogether](https://documentation.sceenic.co)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

To get our sample working, go to our [WEB - tutorial](https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-tutorials/sscale-confluence-web/web-javascript-reactjs2) and follow the instructions.

## Installation
1. Copy `.npmrc.example` to `.npmrc`
2. Replace `YOUR_TOKEN_HERE` with actual token from your private area
3. Run `npm install`

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.  

The page will reload if you make edits.  
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.  
It correctly bundles React in production mode and optimizes the build for the best performance.  

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
