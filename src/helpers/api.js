import axios from 'axios';

export const apiRequest = axios.create({
    baseURL: '',
    headers: {
        'Content-Type': 'application/json',
    }
});

