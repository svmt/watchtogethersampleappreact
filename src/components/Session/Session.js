import React, {useEffect, useRef, useState} from 'react';
import VideoContainer from '../Video/Video';
import WT from '@sscale/wtsdk';
import './Session.css';

/**
 * Watch Together Session
 *
 * WT.Session.onConnected - Method for setting up session when it ready
 * @param {onConnectedCallback} callback - Callback fired when session is ready
 * @callback onConnectedCallback
 * @param {Array} Participants - callback function contain an array of participants in the session
 *
 * WT.SessionListeners.onStreamCreated - Metod will allow you get information about new participants
 * @param {onStreamCreated} callback - Callback fired everytime when participant stream is reade
 * @callback onStreamCreated
 * @param {Object} ParticipantInfo - callback function contain object with participant information
 *
 * WT.ParticipantListeners.onParticipantLeft - Method will allow you get information about participant which left the session
 * @param {onParticipantLeftCallback} callback - Callback fired everytime when participant leave from the session
 * @callback onParticipantLeftCallback
 *
 * WT.Session.disconnect - Method will allow you disconnecting from the Session
 *
 * WT.Session.connect - Method will allow you connecting to the Session
 * @param {Object} SessionSettings - Information for creation the session
 * @param SessionSettings.sessionToken - session token
 * @param SessionSettings.wsUrl - WebSocket Url
 * @param SessionSettings.sessionId - The identifier of the session
 * @param {Object} UserInformation - Information about user
 * @param UserInformation.displayed_name - User ID
 *
 * WT.ErrorListeners.onSessionError - Method will allow you handling session errors
 * @param {onSesionErrorCallback} callback - Callbeck fired everytime when arises error with session
 * @callback onSesionErrorCallback
 * @param {Object} ErrorObject - Information about sesion error
 * @param code - error code
 * @param message - error message
 * @param requestTime - error time in unix timestamp
 */

let globalParticipants = {};
let localParticipant = null;
window.getParticipantsState = () => {
  for (const participantId in globalParticipants) {
    const audioState = WT.Participant.isRemoteAudioEnabled(participantId);
    const videoState = WT.Participant.isRemoteVideoEnabled(participantId);
    console.log(`Participant ${participantId} audio state: ${audioState}`);
    console.log(`Participant ${participantId} video state: ${videoState}`);
  }
}

window.getLocalParticipantState = () => {
    if (localParticipant) {
        const audioState = WT.Participant.isAudioEnabled();
        const videoState = WT.Participant.isVideoEnabled();
        console.log(`Local participant audio state: ${audioState}`);
        console.log(`Local participant video state: ${videoState}`);
    }
}

const WatchTogether = ({history}) => {
  const {session, displayed_name: displayName, config, isViewerMode} = history.location.state;
  const [isMuted, setIsMuted] = useState(false);
  const [isVideoEnabled, setIsVideoEnabled] = useState(false);
  const [participants, setParticipants] = useState([]);
  const participantsRef = useRef(participants);

  useEffect(() => {
    participantsRef.current = participants;
  }, [participants]);

  useEffect(() => {
    startConnecting();
  }, []);


  useEffect(() => {
    WT.SessionListeners.onConnected(() => {
      // Calls when new streams added
      WT.SessionListeners.onStreamCreated(participant => {
        const {stream, participantId, local} = participant;

        if (!local) {
            globalParticipants[participantId] = participant;
        } else {
            localParticipant = participant;
        }


        setParticipants((prevParticipants) => {
          const isParticipantExist = prevParticipants.some( p => p.participantId === participantId);

          if (isParticipantExist) {
            return prevParticipants.map(p => {
                if (p.participantId === participantId) {
                    return participant
                }
                return p;
            })
          }

          return [
            ...participantsRef.current,
            participant,
          ]
        });
      });

      WT.ParticipantListeners.onParticipantLeft((participant) => {
        delete globalParticipants[participant.participantId];
        setParticipants(
            participantsRef.current.filter((p) => p.participantId !== participant.participantId)
        );
      });

      WT.ParticipantListeners.onParticipantMediaStreamChanged((mediaStreamChanged) => {
        console.log(mediaStreamChanged);
      })

      WT.ErrorListeners.onSessionError(event => {
        // Error handling
        if (event.error.code === 206) {
          // Handle full room error
        }
      });

    });
  }, [])

  const handleNavigateBack = () => {
    WT.Session.disconnect();
    history.goBack();
  };

  const startConnecting = () => {
    const {token} = config;
    const connect = isViewerMode ? WT.Session.connectAsViewer : WT.Session.connect;

    connect(token, displayName,
        {
          audio: true,
          video: true,
        },
        {
          // fakeVideo: true,
          // fakeAudio: true,
        });
  };

  const toggleAudio = () => {
    if (WT.Participant.isAudioEnabled()) {
      WT.Participant.disableAudio();
      setIsMuted(true);
    } else {
      WT.Participant.enableAudio();
      setIsMuted(false);
    }
  };

  const toggleVideo = () => {
    if (WT.Participant.isVideoEnabled()) {
      WT.Participant.disableVideo();
      setIsVideoEnabled(true);
    } else {
      WT.Participant.enableVideo();
      setIsVideoEnabled(true);
    }
  };

  const shareScreen = () => {
    WT.Participant.startScreenSharing('test-video');
  };

  return (
    <div className='main-container'>
      <div className='main-container_header'>
        <div onClick={handleNavigateBack} className='navigate-back'>
          <div className='navigate-back-icon'/>
        </div>
        <span>Powered by </span>
        <div className='header__icon'/>
      </div>
      <div className='content-wrapper'>
        <div className='streams-container'>
          {participants.length
            ? participants.map(user =>
              user.stream ? (
                <VideoContainer
                  key={user.participantId}
                  stream={user.stream}
                  participant={user.participantId}
                  isLocal={user.local}
                  isMuted={user.local ? isMuted : null}
                  isVideoEnabled={user.local ? isVideoEnabled : null}
                  toggleAudio={toggleAudio}
                  toggleVideo={toggleVideo}
                />
              ) : null
            )
            : null}
        </div>
      </div>
    </div>
  );
};

export default WatchTogether;
