import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import {apiRequest} from '../../helpers/api';
import './Login.css';

const Login = () => {
    const history = useHistory();
    const [formData, setFormData] = useState({
        token: '',
        displayName: '',
        isViewerMode: false
    });

    const handleFormInputs = (e) => {
        const {name, value} = e.target;
        setFormData({...formData, [name]: value});
    };

    const handleViewerMode = (e) => {
        e.persist()
        setFormData({ ...formData, isViewerMode: e.target.checked });
    }

    const handleSubmit = async (e, viewer = false) => {
        e.preventDefault()
        const { displayed_name, token} = formData;

        try {
            history.push('/session', {
                ...formData,
                viewer,
                config: {token},
                displayed_name,
            });
        } catch (error) {
            console.log('error ', error);
        }
    };

    return (
        <div className='login-wrapper'>
            <div id='join-title'>Authentication</div>
            <div className='login-container'>
                <form className='login-form' onSubmit={handleSubmit}>
                    <input
                        type='text'
                        id='displayed_name'
                        name='displayed_name'
                        onChange={handleFormInputs}
                        placeholder='Displayed Name'
                        autoComplete='true'
                        required
                    />
                    <input
                        type='text'
                        id='token'
                        name='token'
                        onChange={handleFormInputs}
                        placeholder='Auth Token'
                        autoComplete='true'
                        required
                    />


                    <button type='submit' className='join-btn'>
                        JOIN SESSION!
                    </button>
                </form>
            </div>
        </div>
    );
};

export default Login;
