import React from 'react';
import ReactDOM from 'react-dom';
import {Route, BrowserRouter as Router, Redirect} from 'react-router-dom';
import Session from './components/Session/Session';
import Login from './components/Login/Login';
import './index.css';

ReactDOM.render(
    <React.StrictMode>
        <Router>
            <Route exact path='/' component={Login}/>
            <Route path='/session' component={Session}/>
            <Route render={() => <Redirect to='/'/>} path='*'/>
        </Router>
    </React.StrictMode>,
    document.getElementById('root')
);

